# Cellctl-rs experiment

> 🗒 @jcstephenson is currently working in this repository as a playground
> for ideas on creating cell tooling as a part of
> https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues/25183.
>
> I am working in Rust as a language I can prototype in quickest,
> though the end result of the experiments is likely to be a Go-based
> tool.

This repository codifies the contents of the tenant-schema repository,
which at time of writing is internal, so I am opting to keep this
repository at the same visibility.

> As an experimental repository there are unused and incomplete type definitions

## Functionality

- [x] Template a cell definition given
- [x] Validate a cell definition against:
  - [x] tenant schema definition
  - [x] jsonschema definition
