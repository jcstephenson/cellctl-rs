use std::path::PathBuf;

pub fn current_repo_dir() -> Option<PathBuf> {
    match git2::Repository::discover(".") {
        Ok(repo) => repo.workdir().map(PathBuf::from),
        _ => None,
    }
}
