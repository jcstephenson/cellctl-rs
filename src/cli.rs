use std::path::{Path, PathBuf};
use std::collections::BTreeMap;
use clap::{Parser, Subcommand};

use jrsonnet_evaluator::{gc::GcHashMap, function::TlaArg, error::ErrorKind};
use jrsonnet_parser::{ParserSettings, Source};

use serde::{Deserialize, Serialize};

use crate::cell::model::Cell;
use crate::cell::{FSLoader, Searcher, SearcherError};


#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[clap(subcommand)]
    command: Commands,
}

impl Cli {
    pub fn run() -> Result<(), CommandError> {
        let cli = Self::parse();
        cli.command.run(&cli)
    }
}

#[derive(clap::ValueEnum, Clone, Debug)]
pub enum OutputFormat {
    YAML,
    JSON,
}

impl OutputFormat {
    pub fn format<T: Serialize>(&self, data: &T) -> Result<String, Box<dyn std::error::Error>> {
	Ok(match self {
	    OutputFormat::YAML => serde_yaml::to_string(data)?,
	    OutputFormat::JSON => serde_json::to_string(data)?,
	})
    }
}


#[derive(Subcommand, Debug)]
pub enum Commands {
    /// Get a cell metadata
    Get {
	/// Path to look for cells within
	#[clap(long, default_value="cells")]
	path: PathBuf,
	/// ID of a single cell instance to find
	name: Option<String>,
	/// Label selector to filter instances
	#[clap(short='l', long, value_delimiter=',')]
	label_selector: Vec<String>,
	/// format
	#[clap(short='o', long, value_enum, default_value_t=OutputFormat::YAML)]
	format: OutputFormat,
    },
    /// Template
    Template {
	/// Cell definition file
	#[clap(short, long)]
	file: Option<PathBuf>,
	/// Path to look for cells within
	#[clap(long, default_value="cells")]
	cells_path: PathBuf,
	/// Cell name to render
	name: Option<String>,
	/// Base template file
	#[clap(long)]
	template: Option<PathBuf>,
	/// Schema file to validate against, if provided, jsonschema
	/// will validate the tenant model using the schema
	#[clap(long)]
	schema_file: Option<PathBuf>,
	#[clap(short='D', long)]
	debug: bool,
    },
}

#[derive(Debug, thiserror::Error)]
pub enum CommandError {
    #[error("no unique instance found")]
    NoUniqueInstanceFound,

    #[error("failed parsing: {0}")]
    FailedParsing(#[from] ParsingError),

    #[error("failed json serialization/deserialization")]
    FailedJSONSerde(#[from] serde_json::Error),

    #[error("failed yaml serialization/deserialization")]
    FailedYAMLSerde(#[from] serde_yaml::Error),

    #[error("failed rendering jsonnet: {0}")]
    FailedJsonnetRender(#[from] crate::jsonnet::RenderError),

    #[error("failed validation: {0}")]
    FailedValidation(#[from] ValidationError),

    #[error("error finding a cell instance {0}")]
    FailedCellFinding(#[from] SearcherError),

    #[error("no cell specified")]
    NoCellSpecified,

    #[error("unknown error: {0}")]
    Unknown(#[from] Box<dyn std::error::Error>),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Items<T: Sized> {
    items: Vec<T>
}

impl Commands {
    fn run(&self, _cli: &Cli) -> Result<(), CommandError> {
        match self {
	    Commands::Get {path, name, label_selector, format } => {
		let mut cells_iter = FSLoader::new(true, path);

		if let Some(name) = name {
		    let cell = cells_iter.find_by_name(name)?;
		    println!("{}", format.format(&cell)?);
		} else {
		    let labels: BTreeMap<&str, &str> =
			label_selector.iter()
				      .filter_map(|label| {
					  if let Some((key, value)) = label.split_once('=') {
					      Some((key, value))
					  } else { None }
				      })
				      .collect();

		    let items = Items {
			items: cells_iter
			    .find_by_labels(&labels)
			    .collect()
		    };

		    println!("{}", format.format(&items)?);
		};

		Ok(())
	    },
	    Commands::Template { file, cells_path, name, schema_file, template, debug } => {
		let cell: Cell = match (file, name) {
		    (Some(f), _) => serde_yaml_read_from_path(f)?,
		    (None, Some(n)) => FSLoader::new(true, cells_path).find_by_name(n)?,
		    _ => Err(CommandError::NoCellSpecified)?

		};
		let template = template.clone()
		    .or_else(|| cell.metadata.template.clone().map(|t| t.into()))
		    .ok_or(ParsingError::MissingTemplate)?;

		let model: serde_json::Value = render_template(&cell, &template)?;

		if let Some(schema_path) = schema_file {
		    validate_schema(schema_path, &serde_json::to_value(&model)?, *debug)?;
		};

		let model_json = serde_json::to_string_pretty(&model);
		println!("{}", model_json?);

		Ok(())
	    },
        }
    }
}

#[derive(Debug, thiserror::Error)]
pub enum ValidationError {
    #[error("failed to serialize or deserialize data: {0}")]
    FailedJSONSerialization(#[from] serde_json::Error),
    #[error("failed loading: {0}")]
    FailedLoading(#[from] ParsingError),
}

pub fn validate_schema(schema_path: &Path, model: &serde_json::Value, debug: bool) -> Result<(), ValidationError> {
    let mut schema_def: BTreeMap<String, serde_json::Value> = serde_json_read_from_path(schema_path)?;
    // Remove $id to avoid a URL parsing error in the jsonschema library
    schema_def.remove("$id");
    let schema_def = serde_json::to_value(schema_def)?;
    if debug {
	println!("{}", schema_def)
    }
    let schema = jsonschema::JSONSchema::options().compile(&schema_def);

    if let Err(error) = &schema {
	eprintln!("{}", error.schema_path);
	eprintln!("Validation error: {}", error);
	eprintln!("Instance path: {}", error.instance_path);
    }

    let schema = schema.expect("A valid schema");
    let result = schema.validate(model);

    if let Err(errors) = result {
	for error in errors {
	    eprintln!("{}", error.schema_path);
	    eprintln!("Validation error: {}", error);
	    eprintln!("Instance path: {}", error.instance_path);
	};
    }

    Ok(())
}


#[derive(Debug, thiserror::Error)]
pub enum ParsingError {
    #[error("unknown template")]
    MissingTemplate,

    #[error("failed to open file: {0}")]
    FailedFileOpen(#[from] std::io::Error),

    #[error("failed to parse YAML: {0}")]
    FailedYAMLParsing(#[from] serde_yaml::Error),

    #[error("failed to parse JSON: {0}")]
    FailedJSONParsing(#[from] serde_json::Error),
}


pub fn serde_yaml_read_from_path<T: for<'de> serde::Deserialize<'de>>(loc: &Path) -> Result<T, ParsingError> {
    let file = std::fs::File::open(loc)?;
    let reader = std::io::BufReader::new(file);

    let out: T = serde_yaml::from_reader(reader)?;

    Ok(out)
}

pub fn serde_json_read_from_path<T: for<'de> serde::Deserialize<'de>>(loc: &Path) -> Result<T, ParsingError> {
    let file = std::fs::File::open(loc)?;
    let reader = std::io::BufReader::new(file);

    let out: T = serde_json::from_reader(reader)?;

    Ok(out)
}

pub fn render_template<T: for<'de> Deserialize<'de>>(cell: &Cell, template_path: &Path) -> Result<T, crate::jsonnet::RenderError> {
    let cell_code = serde_json::to_string(cell)?;
    let vm = crate::jsonnet::VM::default();

    let mut tlas = GcHashMap::new();
    // Add cell
    let cell_source = Source::new_virtual("<top-level-arg:cell>".into(), cell_code.clone().into());
    tlas.insert(
	"cell".into(),
	TlaArg::Code(
	    jrsonnet_parser::parse(
		&cell_code.clone(),
		&ParserSettings {
		    source: cell_source.clone(),
		},
	    )
		.map_err(|e| ErrorKind::ImportSyntaxError {
		    path: cell_source,
		    error: Box::new(e),
		})?,
	)
    );

    let result: T = vm.render_function_file(template_path, jrsonnet_evaluator::manifest::JsonFormat::minify(), &tlas)?;

    Ok(result)
}
