use std::path::{Path, PathBuf};
use std::ffi::OsString;
use std::collections::{BTreeMap, VecDeque};
use crate::cell::Cell;

#[derive(Debug, thiserror::Error)]
pub enum LoadError {
    #[error("path does not exist: {0}")]
    PathDoesntExist(PathBuf),
    #[error("path is not a file: {0}")]
    PathNotFile(PathBuf),
    #[error("failed to open file: {0}")]
    FailedFileOpen(#[from] std::io::Error),
    #[error("failed to deserialize definition: {0}")]
    FailedSerialization(#[from] serde_yaml::Error),
    #[error("unknown error: {0}")]
    Unknown(#[from] Box<dyn std::error::Error>),
}

/// A loader implementation that will read cell definitions from the
/// local file system.
///
/// This can optionally traverse the filesystem to open a stream of
/// cell definitions, agnostic to the filesystem structure.
#[derive(Debug)]
pub struct FSLoader {
    /// Recursively
    recursive: bool,
    paths: VecDeque<PathBuf>,
}

impl FSLoader {
    /// Create a new instance of FSLoader
    ///
    /// Due to the intended usage as an iterator, you can only set the
    /// path at creation time
    pub fn new(recursive: bool, path: &Path) -> Self {
	let mut paths = VecDeque::new();
	paths.push_back(path.into());

	Self {
	    paths,
	    recursive,
	}
    }

    /// Enable or disable recursive search for Cells
    pub fn with_recursive(self, recursive: bool) -> Self {
	Self {
	    recursive,
	    ..self
	}
    }
}

impl Iterator for FSLoader {
    type Item = Cell;

    fn next(&mut self) -> Option<Self::Item> {
	let path: PathBuf = self.paths.pop_front()?;
	if self.recursive && path.is_dir() {
	    if let Ok(dir_entry) = std::fs::read_dir(&path) {
		for entry in dir_entry.flatten() {
		    let entry_path = entry.path();
		    if let Some(ext) = entry_path.extension().map(|ext| ext.to_ascii_lowercase()) {
			if [OsString::from("yaml"), OsString::from("yml")].contains(&ext) {
			    self.paths.push_back(entry_path);
			}
		    } else if entry_path.is_dir() {
			self.paths.push_back(entry_path);
		    };
		}
	    };

	    self.next()
	} else {
	    let file = std::fs::File::open(path);
	    if let Err(e) = file {
		eprintln!("failed to open file: {0}", e);
		return self.next()
	    };

	    let reader = std::io::BufReader::new(file.unwrap());

	    match serde_yaml::from_reader(reader) {
		Ok(cell) => cell,
		Err(e) =>  {
		    eprintln!("failed to open file: {0}", e);
		    self.next()
		}
	    }
	}
    }
}

/// Errors that occur when attempting to find and filter cells using
/// the Searcher
#[derive(Debug, thiserror::Error)]
pub enum SearcherError {
    #[error("no cell found for {0}")]
    NoCellFound(String),
    #[error("multiple cells found")]
    MultipleCellsFound(Vec<Cell>),
}

/// Search through additional
pub trait Searcher {
    ///
    fn find_by_name(&mut self, name: &str) -> Result<Cell, SearcherError>;
    fn find_by_labels(&mut self, labels: &BTreeMap<&str, &str>) -> impl Iterator<Item=Cell>;
}

/// Implement CellLoader for all iterators of Cells
///
/// This provides us with some additional methods on those iterators
/// with which to find cells.
impl <T>Searcher for T
where T: Iterator<Item=Cell> {
    fn find_by_name(&mut self, name: &str) -> Result<Cell, SearcherError> {
	let mut cells: Vec<Cell> = self.filter(|cell| cell.metadata.name == name).collect();

	// Check we have found a unique cell
	match cells.len() {
	    0 => Err(SearcherError::NoCellFound(format!("name={}", name))),
	    1 => Ok(cells.pop().expect("unique cell found")),
	    _ => Err(SearcherError::MultipleCellsFound(cells))
	}
    }

    fn find_by_labels(&mut self, labels: &BTreeMap<&str, &str>) -> impl Iterator<Item=Cell> {
	self.filter(|cell| {
	    labels.iter().all(|(key, value)| {
		if let Some(v) = cell.metadata.labels.get(&key.to_string()) {
		    v == value
		} else { false }
	    })
	})
    }
}
