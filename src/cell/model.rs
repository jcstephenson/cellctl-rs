use std::collections::BTreeMap;
use serde::{Serialize, Deserialize};

/// A cell definition
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Cell {
    /// Additional metadata
    pub metadata: CellMetadata,
    /// Cell Spec
    pub spec: CellSpec,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct CellMetadata {
    /// The identifier of the cell
    pub name: String,
    /// Optional template specifier
    ///
    /// If this is not set then the template must be provided
    /// externally.
    pub template: Option<String>,
    /// Additional annotations
    ///
    /// To be used for defining features and passing additional
    /// context through to the cell template.
    pub annotations: BTreeMap<String, String>,
    /// Additional labels
    ///
    /// Used to add human understandable context to cell instances.
    /// Can be used to perform searches across cell instances.
    pub labels: BTreeMap<String, String>,
}

impl TryFrom<&Cell> for serde_json::Value {
    type Error = serde_json::Error;
    fn try_from(value: &Cell) -> Result<Self, Self::Error> {
	serde_json::to_value(value)
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct CellSpec {
    /// Primary GCP project ID
    pub project_id: String,
    /// The region we want this cell to be provisioned within
    pub region: String,
    /// Reference architecture
    pub reference_architecture: String,
    /// Instrumentor version
    #[serde(skip_serializing_if="Option::is_none")]
    pub instrumentor_version: Option<String>,
    /// GitLab version
    pub gitlab_version: Option<String>,
    /// Prerelease version
    pub prerelease_version: Option<String>,
}
