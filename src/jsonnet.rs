use std::path::{Path, PathBuf};
use std::collections::BTreeMap;

use jrsonnet_evaluator::trace::{ExplainingFormat, PathResolver, TraceFormat};
use jrsonnet_evaluator::{gc::GcHashMap, function::TlaArg, IStr};
use jrsonnet_evaluator::{tb, FileImportResolver, State};
use jrsonnet_evaluator::manifest::ManifestFormat;
use serde::Deserialize;

#[derive(Default)]
pub struct VM {
    pub jpaths: Vec<PathBuf>,
    pub external_strings: BTreeMap<String, String>,
}

impl VM {
    fn new_state(&self, path: &Path) -> State {
	let s = State::default();

        let ctx =
            jrsonnet_stdlib::ContextInitializer::new(s.clone(), PathResolver::new_cwd_fallback());

        // Add default ext_strs first
        for (key, val) in self.external_strings.iter() {
            ctx.add_ext_str(key.into(), val.into());
        }

        s.settings_mut().context_initializer = tb!(ctx);
        s.set_import_resolver(FileImportResolver::new(self.jpaths(path)));

        s

    }

    fn jpaths(&self, file: &Path) -> Vec<PathBuf> {
        let file_parent = file
            .parent()
            .expect("we must be executing a file that exists");
        let mut out = vec![PathBuf::from("."), file_parent.into()];

        out.extend_from_slice(&self.jpaths);
        out.extend_from_slice(&self.default_jpaths(file));

        out
    }

    fn default_jpaths(&self, file: &Path) -> Vec<PathBuf> {
        let boundary = crate::git::current_repo_dir().unwrap_or_else(|| PathBuf::from("."));

        let mut out = Vec::new();

        for path in file.ancestors() {
            // Break if we match our current boundary
            //
            // We assume here that we will either always be in a git
            // repo, or that we will only render paths underneath us.
            if boundary == path {
                break;
            }

            if path.join("jsonnetfile.json").exists() {
                out.append(&mut vec![
                    path.join("lib"),
                    path.join("vendor"),
                    path.join("condor"),
                ]);
            }
        }

        out
    }

    pub fn render_function_file<T: for<'de> Deserialize<'de>>(&self, file: &Path, format: impl ManifestFormat, tlas: &GcHashMap<IStr, TlaArg>) -> Result<T, RenderError> {
	let state = self.new_state(&PathBuf::from("."));
	let val = state.import(file)?;
	let val = jrsonnet_evaluator::apply_tla(state, tlas, val)?;

	let out = val.manifest(format)
           .map_err(|err: jrsonnet_evaluator::Error| {
               ExplainingFormat {
                   resolver: PathResolver::new_cwd_fallback(),
                   max_trace: 10,
               }.format(&err)
		.expect("formatting error")
           })?;

	Ok(serde_json::from_str(&out)?)

    }
}

#[derive(Debug, thiserror::Error)]
pub enum RenderError {
    #[error("serialisation error: {0}")]
    FailedSerialization(#[from] serde_json::Error),
    #[error("failed evaluation: {0}")]
    FailedEvaluation(#[from] jrsonnet_evaluator::Error),
    #[error("failed parsing: {0}")]
    FailedParsing(#[from] jrsonnet_evaluator::error::ErrorKind),
    #[error("manifest error: {0}")]
    FailedManifest(String),
    #[error("unknown error: {0}")]
    UnknownError(#[from] Box<dyn std::error::Error>),
}

impl From<String> for RenderError {
    fn from(value: String) -> Self {
	RenderError::FailedManifest(value)
    }
}
