use cellctl::cli::Cli;

fn main() {
    if let Err(err) = Cli::run() {
	eprintln!("encountered an error: {}", err)
    }
}
