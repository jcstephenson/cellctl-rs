function (cell) {
  optionalField(field, mappedName=field): (
    if std.objectHas(cell, field) && cell[field] != null
    then mappedName
    else null
  ),
  option(field, default): (
    if self.optionalField(field) != null
    then cell[field]
    else default
  ),
}
