function(cell)
  local utils = (import './utils.libsonnet')(cell.spec);

  {
    tenant_id: cell.metadata.name,
    instrumentor_version: utils.option('instrumentor_version', '16.222.0'),
    managed_domain: '%s.cells.gitlab.io' % cell.metadata.name,
    reference_architecture: utils.option('reference_architecture', 'arch25k'),
    perform_qa: false,
    gitlab_version: utils.option('gitlab_version', '16.10.0'),
    [utils.optionalField('prerelease_version')]: cell.spec.prelease_version,
    cloud_provider: 'gcp',
    gcp_project_id: cell.spec.project_id,
    amp_gcp_project_id: 'amp-amp12',
    dns_aws_account_id: '123412341234',
    primary_region: cell.spec.region,
    backup_region: cell.spec.region,
    gcp_onboarding_state_region: cell.spec.region,
    site_regions: [
      cell.spec.region,
    ],
    service_account_impersonation_members: [],
  }
